<?php


namespace Adept\dz2\Tests;


use Adept\dz2\Facades\TransactionService;
use Adept\dz2\Models\Account;
use Adept\dz2\Models\PaymentObject;


class TransTest extends TestCase
{

    private $a, $b;
    private $obj;

    protected function setUp(): void
    {
        parent::setUp();
        $this->a = Account::create(['name' => 'me']);
        $this->b = Account::create(['name' => 'you']);
        $this->obj = PaymentObject::create(['name' => 'market']);
    }

    /**
     * Тест на привязку транзакций к объектам и аккаунтам.
     * Совершаются две транзакции. С помощью получателей этих транзакций, журнала и связей
     * проверяется , что транзации были отправлены с одного аккаунта.
     */
    public function test_relationship()
    {
        TransactionService::transact($this->a, $this->b, 'money transaction', 2000);
        TransactionService::transact($this->a, $this->obj, 'buying', 100);


        $this->assertTrue(
            $this->b->notes()->first()->journal->posting()->first()->target_id ==
            $this->obj->notes()->first()->journal->posting()->first()->target_id
        );

    }

    /**
     * Тест на пробный баланс - сумма всех транзаций должна быть равна 0
     **/
    public function test_total_system_balance()
    {
        $this->assertTrue(TransactionService::systemBalance() == 0);

        TransactionService::transact($this->a, $this->b, 'money transaction', 2000);

        $this->assertTrue(TransactionService::systemBalance() == 0);
    }

    /**
     * Тест на баланс аккаунт
     * Он должен быть равен -100, так как была совершена транзакция
     */
    public function test_account_balance()
    {

        TransactionService::transact($this->a, $this->obj, 'buying', 100);

        $this->assertTrue(TransactionService::accountBalance($this->a) == -100);
    }


}
