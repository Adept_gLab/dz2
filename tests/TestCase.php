<?php


namespace Adept\dz2\Tests;


use Adept\dz2\Providers\TransactionServiceProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class TestCase extends \Orchestra\Testbench\TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate');

    }

    protected function getPackageProviders($app)
    {
        return [
             TransactionServiceProvider::class
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'TransactionService' => 'Adept\dz2\Facades\TransactionService'
        ];
    }


}
