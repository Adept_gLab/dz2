<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostingTable extends Migration
{
    /**
     * Таблица двойной записи
     * target_id - цель
     * target_type - тип - аккаунт или объект
     * journal_id - запись в журнале
     * value - значение
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posting', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('target_id');
            $table->string('target_type');
            $table->unsignedBigInteger('journal_id');
            $table->integer('value');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posting');
    }
}
