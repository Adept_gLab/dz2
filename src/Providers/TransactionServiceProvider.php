<?php


namespace Adept\dz2\Providers;


use Illuminate\Support\ServiceProvider;

class TransactionServiceProvider extends ServiceProvider
{
   public function register()
   {
       $this->app->bind('myTransaction', 'Adept\dz2\Services\Transaction');
   }

    public function boot()
    {
        $this->loadMigrationsFrom(realpath(__DIR__ . '/../migrations'));
    }
}
