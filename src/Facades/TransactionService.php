<?php


namespace Adept\dz2\Facades;

use Illuminate\Support\Facades\Facade;

class TransactionService extends Facade
{
   protected static function getFacadeAccessor()
   {
       return 'myTransaction';
   }
}
