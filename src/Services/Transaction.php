<?php


namespace Adept\dz2\Services;


use Adept\dz2\Models\Account;
use Adept\dz2\Models\Journal;
use Adept\dz2\Models\PaymentObject;
use Adept\dz2\Models\Posting;

class Transaction
{
    /**
     * @param $sender Account
     * @param $receiver Account | PaymentObject
     * @param $type string
     * @param $value integer
     *
     * Транзакция с использованием двойной записи.
     * Получателем  $receiver может быть как другой аккаунт,
     * так и объект оплаты. Это достигается засчет morph-связи
     */
    public function transact($sender, $receiver, $type, $value)
    {

        $trans_note = Journal::create(['type' => $type]);

        $debit = new Posting(['value' => -$value]);
        $debit->journal()->associate($trans_note);
        $debit->target()->associate($sender);
        $debit->save();

        $credit = new Posting(['value' => $value]);
        $credit->journal()->associate($trans_note);
        $credit->target()->associate($receiver);
        $credit->save();
    }


    /**
     * @param $account Account
     * @return int
     *
     * Выводит баланс аккаунта
     */
    public function accountBalance($account)
    {
        return $account->notes->sum('value');
    }

    /**
     * @return int
     *
     * Выводит сумму всех транзацкий
     */
    public function systemBalance()
    {
       return Posting::all()->sum('value');
    }


}
