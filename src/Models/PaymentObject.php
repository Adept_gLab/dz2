<?php

namespace Adept\dz2\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentObject
 * @package App\Models
 *
 * Модель объекта оплаты
 */

class PaymentObject extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     * Получение записей в Posting. morph-relation
     */
    public function notes(){
        return $this->morphMany(Posting::class, 'target');
    }
}
