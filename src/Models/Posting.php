<?php

namespace Adept\dz2\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Posting
 * @package App\Models
 *
 * Модель записи.
 * Может содержать в качестве цели Account или PaymentObject
 */

class Posting extends Model
{
    public $timestamps = false;
    protected $table = 'posting';

    protected $fillable = [
        'value', 'target_id', 'target_type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Получение записи в журнале
     */
    public function journal()
    {
        return $this->belongsTo(Journal::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     * Получение цели транзакции - другой акканут или объект
     */
    public function target()
    {
        return $this->morphTo('target');
    }


}
