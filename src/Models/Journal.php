<?php

namespace Adept\dz2\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Journal
 * @package App\Models
 *
 * Модель записи в журнале.
 * Нужна для связи двух записей в одну транзакцию
 * Также хранит тип перевода.
 */

class Journal extends Model
{
    protected $table = 'journal';


    protected $fillable = [
        'type'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Относящиеся записи
     */
    public function posting(){
        return $this->hasMany(Posting::class);
    }
}
