<?php

namespace Adept\dz2\Models;


use Illuminate\Database\Eloquent\Model;


/**
 * Class Account
 * @package App\Models
 * Модель аккаунта
 */
class Account extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'name'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     * Получение записей в Posting. morph-relation
     */
    public function notes(){
        return $this->morphMany(Posting::class, 'target');
    }


}
